# Made from React/Sass/Redux Boilerplate

Exercise Congestion charge

## Getting Started

To get started, first install all the necessary dependencies with yarn.
```
> yarn install
```

Start the development server (changes will now update live in browser)
```
> yarn start
```

App to check UK vehicle registration plate and to charge because of congestion.
example input: BI54KAR, HE54DKE, XS47KED

/*
 * The users reducer will always return an array of users no matter what
 * You need to return something, so if there are no users then just return an empty array
 * */

export default function () {
    return [
        {
            key: 0,
            term: "Today"
        },
        {
            key: 1,
            term: "Tomorrow"
        },
        {
            key: 2,
            term: "Yesterday"
        }
    ]
}

import {combineReducers} from 'redux';
import PaymentDays from './paymentDays';
import selectPaymentDay from './selectPaymentDay';
import vehicleRegistration from './setVehicleRegistration';
import setFreeTransportType from './setFreeTransportType';

const allReducers = combineReducers({
  dates: PaymentDays,
  selectedDay: selectPaymentDay,
  vehicleRegistration: vehicleRegistration,
  isFree: setFreeTransportType
});

export default allReducers

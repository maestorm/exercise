export const selectPaymentDay = (date) => {
  return {
    type: 'DATE_SELECTED',
    payload: date
  }
};

export const setVehicleRegistration = (code) => {
  return {
    type: 'SET_VEHICLE_REGISTRATION',
    payload: code
  }
};

export const setFreeTransportType = (code) => {
  return {
    type: 'SET_FREE',
    payload: code
  }
};
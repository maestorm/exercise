import 'babel-polyfill';
import React from 'react';
import ReactDOM from "react-dom";
import { Router, Route } from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import allReducers from './reducers';
import Main from './pages/main/main';
import Dates from './pages/dates/dates';
import Summary from './pages/summary/summary';
import Payment from './pages/payment/payment';
import Confirmation from './pages/confirmation/confirmation';
import createBrowserHistory from 'history/createBrowserHistory'

const store = createStore(
    allReducers,
    compose(applyMiddleware(thunk, promise),
    window.devToolsExtension ? window.devToolsExtension() : f => f)
);

const customHistory = createBrowserHistory()

ReactDOM.render(
    <Provider store={store}>
      <Router store={store} history={customHistory}>
        <div>
          <Route exact path="/" component={Main} />
          <Route path="/dates" component={Dates} />
          <Route path="/summary" component={Summary} />
          <Route path="/payment" component={Payment} />
          <Route path="/confirmation" component={Confirmation} />
        </div>
      </Router>
    </Provider>,
    document.getElementById('root')
);

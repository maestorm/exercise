import React, { PureComponent }  from 'react';
import {connect} from 'react-redux'
import Menu from '../../components/menu'

class Confirmation extends PureComponent {
  render() {
    const { selectedDay, dates, vehicleRegistration, isFree } = this.props
    return (
      <div className="w3-content">
        <h1 className="w3-center">Congestion Charge</h1>
        <hr/>
        {typeof vehicleRegistration === 'string' ?
          typeof selectedDay === 'string' ?
            <div>
              <h1 className="w3-center">{!isFree && `£5.00 paid for ${vehicleRegistration}`}</h1>
              {isFree ? <div>
                <h4 className="w3-jumbo w3-center">{`${vehicleRegistration} submitted`}</h4>
                <h4 className="w3-jumbo w3-center">{`for ${dates[selectedDay].term}!`}</h4>
              </div> : <h4 className="w3-jumbo w3-center">for {dates[selectedDay].term}!</h4>}
            </div>
            :
            <h1>Day wasn't selected</h1>
          :
          <h1>Vehicle registration wasn't input yet so we can't calculate charge</h1>
        }
        <br />
        <Menu manuList="" >

        </Menu>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    dates: state.dates,
    selectedDay: state.selectedDay,
    vehicleRegistration: state.vehicleRegistration,
    isFree: state.isFree
  };
}

export default connect(mapStateToProps)(Confirmation);
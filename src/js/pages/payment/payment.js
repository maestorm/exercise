import React, { PureComponent }  from 'react'
import {connect} from 'react-redux'
import { Course } from '../../components/course'
import Menu from '../../components/menu'

class Payment extends PureComponent {
  state = {
    card: null,
    cardNumber: '',
    expirationMonth: '',
    securityCode: '',
    enableConfirmation: false,
    approveNextStep: false
  }

  handleMonth(event) {
    this.setState({expirationMonth: event.target.value})
  }

  setCreditCardNumber(event) {
    if(/\d/.test(event.target.value) && event.target.value.length <= 19) {
      if (event.target.value.length === 4 || event.target.value.length === 9 || event.target.value.length === 14) {
        event.target.value = event.target.value + '-';
      }
    } else {
      event.target.value = '\r'
      event.target.placeholder = 'Enter correct credit card number'
    }
    this.setState({cardNumber: event.target.value})
  }

  selectCard(card) {
    card.target.classList.remove('w3-green')
    card.target.classList.add('w3-green')
    this.setState({ card: card.target.value })
  }

  setSecurityCode(card) {
    if(/\d/.test(card.target.value) && card.target.value.length <= 10) {
      this.setState({securityCode: card.target.value})
    } else {
      card.target.value = '\r'
      card.target.placeholder = 'Enter 10 numbers'
    }
  }

  componentDidUpdate() {
    let numberInputs = this.state.cardNumber.length === 19 && this.state.expirationMonth.length === 7 && this.state.securityCode.length === 10
    if (this.state.card !== null && numberInputs === true) {
      this.setState({enableConfirmation: true})
    }
  }

  render() {
    const cards = ['Visa', 'Mastercard', 'Amex']

    return (
      <div className="w3-content">
        <h1 className="w3-center">Congestion Charge</h1>
        <hr/>
        <h4>Payment section</h4>
        <div className="w3-display-container">
        <br />
        <div className="w3-content w3-display-middle w3-half">
          {cards.map((card) => {
            return this.state.card === card ?
              <input key={card} type="button" onClick={(e) => this.selectCard(e)}
                     className="w3-col w3-third w3-border w3-button w3-center w3-green"
                     value={card}/> :
              <input key={card} type="button" onClick={(e) => this.selectCard(e)}
                     className="w3-col w3-third w3-border w3-button w3-center" value={card}/>
          })}
        </div>
        </div>
        <br />
        <span>Enter Card Number:</span>
        <input
          type="text"
          className="w3-input w3-border w3-border-grey"
          value={this.state.cardNumber}
          onChange={(e) => this.setCreditCardNumber(e)}
        />
        <br />
        <span>Expiry Date:</span>
        <div className="w3-bar">
          <input type="month" name="expiration_month" className="w3-border w3-select w3-quarter" onChange={(e) => this.handleMonth(e)} />
        </div>
        <br />
        <span>Security Code (10 numbers):</span>
        <br />
        <input type="text" onChange={(e) => this.setSecurityCode(e)} className="w3-border w3-input w3-quarter" placeholder="Input your security code" />
        <br /><br /><br />
        <Course />
        <br />
        <Menu path={this.props.history.location.pathname} enableConfirmation={this.state.enableConfirmation} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    dates: state.dates
  };
}

export default connect (mapStateToProps)(Payment);
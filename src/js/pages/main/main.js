import React, { PureComponent }  from 'react';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Input from '../../components/input';
import Submit from '../../components/submit';
import {setVehicleRegistration, setFreeTransportType} from '../../actions/index'

class App extends PureComponent {
  state = {
    vehicleIsSet: false
  }

  onChange = (event) => {
    let inputValue = event.target.value
    const regexp = /([A-H,K-P,R,S,V,W,X,Y]){1}([A-Z]){1}(\d){2}([A-Z]{3})\b/g
    const bikeRegex = /\b[B][I]\d{2}[K][E][A-Z]{1}/ig
    if(regexp.test(inputValue)) {
      event.target.classList.remove('w3-red')
      event.target.classList.add('w3-green')
      this.setState({vehicleIsSet: true})
      this.props.vehicleRegistration(inputValue)
      if (inputValue.length > 7) {
        event.target.classList.remove('w3-green')
        event.target.classList.add('w3-red')
        this.props.isFree(false)
        this.setState({vehicleIsSet: false})
      } if(bikeRegex.test(inputValue)) {
        event.target.classList.remove('w3-red')
        event.target.classList.add('w3-green')
        this.props.isFree(true)
        if (inputValue.length > 7) {
          event.target.classList.remove('w3-green')
          event.target.classList.add('w3-red')
          this.props.isFree(false)
          this.setState({vehicleIsSet: false})
        }
      } else {
        this.props.isFree(false)
      }
    } else if(inputValue.length > 6) {
      event.target.classList.add('w3-red')
      event.target.placeholder = 'Input exact 7 characters of correct vehicle registration'
      event.target.value = '\r'
      this.props.isFree(false)
      this.setState({vehicleIsSet: false})
    }
    event.target.placeholder = 'Input exact 7 characters of correct vehicle registration'
  }

  render() {
    return (
      <div className="w3-content">
        <br />
        <h1 className="w3-center">Congestion Charge</h1>
        <hr/>
        <form onSubmit={() => {this.props.history.push('/dates')}}>
          <p>Enter vehicle registration</p>
          <Input onChange={(e) => this.onChange(e)} />
          <br />
          {this.state.vehicleIsSet ?
          <Submit /> :
          <Submit isDisabled={true} />
          }
        </form>
      </div>
    )
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators(Object.assign({},{vehicleRegistration: setVehicleRegistration}, {isFree: setFreeTransportType}), dispatch);
}

export default connect (null, matchDispatchToProps)(App);

import React, { PureComponent }  from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {selectPaymentDay} from '../../actions/index'
import { Course } from '../../components/course'
import Menu from '../../components/menu'

class Dates extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      approveNextStep: false
    }
    this.handleCheckbox = this.handleCheckbox.bind(this)
  }

  handleCheckbox(checkbox) {
    this.props.selectDay(checkbox.target.id)
    this.setState({approveNextStep: true})
  }

  render() {
    if (this.props.selectedDay) {
      this.setState({approveNextStep: true})
    }
    return (
      <div className="w3-content">
        <h1 className="w3-center">Congestion Charge</h1>
        <hr/>
        <div>
          {this.props.dates.map((day) =>
            <span key={day.key}>
              <input type="radio" onChange={e => {
                this.handleCheckbox(e)
              }} id={day.key} checked={parseInt(this.props.selectedDay) === day.key} />
              <label> Pay {day.term}</label>
              <br/>
            </span>
          )}
        </div>
        <br />
        <Course />
        <br />
        <Menu path={this.props.history.location.pathname} approveNextStep={this.state.approveNextStep} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    dates: state.dates,
    selectedDay: state.selectedDay
  };
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectDay: selectPaymentDay}, dispatch);
}

export default connect (mapStateToProps, matchDispatchToProps)(Dates);
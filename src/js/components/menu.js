import React, { PureComponent } from 'react';
import {connect} from 'react-redux'
import { NavLink } from 'react-router-dom';

/** NavLink could be separated into HOC */

class WrapperNavLink extends PureComponent {
  render() {
  const { path } = this.props.data
  const { item } = this.props
  const { approveNextStep } = this.props
  const { enableConfirmation } = this.props
  const isFree = this.props.isFree
    if(!approveNextStep && item !== 'Dates' && path === '/dates') {
      return <NavLink to="#"
                      className='w3-bar-item w3-button w3-disabled'
                      activeClassName="w3-gray w3-text-white">{item}</NavLink>
    } else if (approveNextStep && path === '/dates') {
      if(item === 'Summary') {
        return <NavLink to={'/' + item.toLowerCase()}
                        className='w3-bar-item w3-button'
                        activeClassName="w3-gray w3-text-white">{item}</NavLink>
      } else {
        return <NavLink to="#"
                        className='w3-bar-item w3-button w3-disabled'
                        activeClassName="w3-gray w3-text-white">{item}</NavLink>
      }
    } else if (!isFree && path === '/summary'){
      if(item !== 'Summary' && item !== 'Dates' && item !== 'Payment') {
        return <NavLink to="#"
                        className='w3-bar-item w3-button w3-disabled'
                        activeClassName="w3-gray w3-text-white">{item}</NavLink>
      } else {
        return <NavLink to={'/' + item.toLowerCase()}
                        className='w3-bar-item w3-button'
                        activeClassName="w3-gray w3-text-white">{item}</NavLink>
      }
    } else if (path === '/payment'){
        if(enableConfirmation !== true && item === 'Confirmation') {
          return <NavLink to="#"
                          className='w3-bar-item w3-button w3-disabled'
                          activeClassName="w3-gray w3-text-white">{item}</NavLink>
        } else {
          return <NavLink to={'/' + item.toLowerCase()}
                          className='w3-bar-item w3-button'
                          activeClassName="w3-gray w3-text-white">{item}</NavLink>
        }
    } else {
      return <NavLink to={'/' + item.toLowerCase()}
                      className='w3-bar-item w3-button'
                      activeClassName="w3-gray w3-text-white">{item}</NavLink>
    }
  }
}

class Menu extends PureComponent {
  render() {
    const { isFree, approveNextStep }  = this.props
    let menu = ['Dates', 'Summary', 'Payment', 'Confirmation']
    if(isFree) {
      menu.splice(2,1)
    }
    return (
      <div className="w3-bar w3-light-grey">
        {menu.map((item, count) => {
          return <WrapperNavLink
            key={count}
            item={item}
            isFree={isFree}
            enableConfirmation={this.props.enableConfirmation}
            approveNextStep={approveNextStep}
            data={this.props}
          />
        })}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    dates: state.dates,
    vehicleRegistration: state.vehicleRegistration,
    isFree: state.isFree
  };
}

export default connect(mapStateToProps)(Menu);

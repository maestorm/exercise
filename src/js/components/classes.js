const styles = [`
  w3-bar-item, 
  w3-button, 
  w3-disabled,
  w3-light-gray,
  w3-display-left,
  w3-gray,
  w3-text-white,
  w3-input,
  w3-border,
  w3-border-gray,
  w3-green,
  w3-panel,
  w3-display-container,
  w3-jumbo,
  w3-center`
]

const arr = styles[0].split(",")

export default arr;
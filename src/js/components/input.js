import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import arr from '../components/classes';

class Input extends PureComponent {
  render() {
    let storedInputValue = this.props.vehicleRegistration
    if(typeof storedInputValue !== 'string') {
      storedInputValue=null
    }

    return (
      <div>
        <input type="text"
          onChange={(e) => this.props.onChange(e)}
          className={`${arr[7] + arr[8] + arr[9]} ${storedInputValue && arr[10]}`}
          placeholder={storedInputValue}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    dates: state.dates,
    vehicleRegistration: state.vehicleRegistration
  };
}

export default connect(mapStateToProps)(Input);

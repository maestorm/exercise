import React, { PureComponent } from 'react';

class Submit extends PureComponent {
  render() {
  const { isDisabled } = this.props
  const buttonClass = 'w3-button w3-light-gray'
    return (
      <div>
        {!isDisabled ?
          <button type="submit" value="Submit" className={buttonClass}>Submit</button>
          :
          <button type = "submit" value="Submit" className={buttonClass} disabled>Submit</button>
        }
      </div>
    );
  }
}

export default Submit

import React from 'react';
import { withRouter } from 'react-router-dom'

export const Course = withRouter((props) => {
  /** in case need change of the course component input here */
  return (
      <div className="w3-panel w3-display-container">
        <button onClick={props.history.goBack} className='w3-bar-item w3-button w3-display-left w3-light-gray'>&laquo; Back</button>
        <button onClick={props.history.goForward} className='w3-bar-item w3-button w3-display-right w3-light-gray'>Next &raquo;</button>
      </div>
    )
  }
)